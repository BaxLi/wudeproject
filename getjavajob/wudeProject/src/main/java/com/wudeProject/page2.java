package com.wudeProject;


@WebServlet(name = "/page2",urlPatterns ={"/page2"})

public class page2 extends HttpServlet {
    private static int counter = 0;

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        //TODO ask Emin about user authorisation - move this check to the preprocessing-FILTER ?

        HttpSession session = request.getSession();

        StringBuilder str=new StringBuilder("<br> Start reading cookies : </br>"+" 0");
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("cookieName")) {
                    str.append("\n").append(cookie.getName()+" - ").append(cookie.getValue());
                }
            }
        }
        response.getWriter().println(str.toString()+"PAGE2");
    }
}
