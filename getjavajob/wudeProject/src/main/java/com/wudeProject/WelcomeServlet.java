package com.wudeProject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "/welcome",urlPatterns ={"/"})

public class WelcomeServlet extends HttpServlet {
    private static int counter=0;

    protected void doGet(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        //TODO ask Emin about user authorisation - move this check to the preprocessing-FILTER ?

        HttpSession session= request.getSession();
        Integer userID=(Integer) session.getAttribute("userID");
        String userRole= (String) session.getAttribute("userRole");
        if ((userID==null||userRole==null)&&(!request.getRequestURI().contains("loginpage.jsp"))) {
            request.getRequestDispatcher("/loginpage.jsp").forward(request, response);
        }
        else {
//        response.getWriter().println("Welcome file welcome you"+request.getParameter("userRole").toString());
//        response.getWriter().println("Embedded TOMCAT !@!");
//            request.forward(request, response);
        }

    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        HttpSession session= request.getSession();
        session.setAttribute("userID",25);
        session.setAttribute("userRole","Batman");
        Cookie cookie = new Cookie(session.getAttribute("userID").toString(),session.getAttribute("userRole").toString());
        cookie.setMaxAge(60*60); //1 hour
        response.addCookie(cookie);

        response.getWriter().println("doPost Answered Session ID "+session.getId());
        }
    }
