package com.getjavajob.wudeProject.dao_sql;

import javafx.application.Application;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class ConnectToDB {
    // JDBC URL, username and dbpassword of MySQL server
    private static String url = "jdbc:mysql://localhost:3306/wude_competition";
    private static String dbuser = "root";
    private static String dbpassword = "root";

    // JDBC variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs = null;

    public static ResultSet connectTo() {

//        ConnectToDB.readPropertiesFromFile();

        String query = "select count(*) from peoples";
        try {
            // opening database connection to MySQL server
            System.out.println(url+" / "+ dbuser+" / "+ dbpassword);
            con = DriverManager.getConnection(url, dbuser, dbpassword);
            // getting Statement object to execute query
            stmt = con.createStatement();
            // executing SELECT query
            rs = stmt.executeQuery(query);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection ,stmt and resultset here
            try {
                con.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                stmt.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                rs.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        return rs;
    }

    private static void readPropertiesFromFile() {
        Properties prop = new Properties();
        try {
            //load a properties file from class path, inside static method
            prop.load(Application.class.getClassLoader().getResourceAsStream("config.properties"));

            //get the property value and print it out
            url = prop.getProperty("database");
            dbuser = prop.getProperty("dbuser");
            dbpassword = prop.getProperty("dbpassword");

        } catch (IOException ex) {
//            ex.printStackTrace();
        }
        finally{
            System.out.println("step");
        }
    }
}

