package com.getjavajob.wudeProject.ui_web.interfaces;

import java.time.LocalDate;


public interface ICompetition {


    public String setCompetitionFullName(String a); //Full name of ui_web.competition

    public String getCompetitionFullName();

    public String setPrintCompetitionFullName(String a); //Category of ui_web.competition

    public String getPrintCompetitionFullName();

    public LocalDate setStartCompetitionDate(LocalDate a);

    public LocalDate getStartCompetitionDate();

    public LocalDate setEndCompetitionDate(LocalDate a);

    public LocalDate getEndCompetitionDate();

    public IReferee setCompetitionGeneralReferee(IReferee a);

    public IReferee getCompetitionGeneralReferee();

    public IReferee setCompetitionGeneralSecretary(IReferee a);

    public IReferee getCompetitionGeneralSecretary();

    public Boolean setIsCompetitionGoing(Boolean a);

    public Boolean getIsCompetitionGoing();



}
