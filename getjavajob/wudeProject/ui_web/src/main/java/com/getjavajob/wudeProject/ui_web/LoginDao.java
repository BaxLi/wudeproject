package com.getjavajob.wudeProject.ui_web;


import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class LoginDao {

    public static boolean validate(String name, String pass) {
        boolean status = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/login_db", "root", "root");

            PreparedStatement ps = con.prepareStatement(
                    "select * from login_db where username=? and userpassword=?");
            ps.setString(1, name);
            ps.setString(2, pass);

            ResultSet rs = ps.executeQuery();
            status = rs.next();

        } catch (Exception e) {
            System.out.println(e);
        }
        return status;

    }

}
